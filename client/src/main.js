import { createApp } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import installElementPlus from './plugins/element'

const app = createApp(App)
app.use(VueAxios, axios)
installElementPlus(app)
app.mount('#app')
