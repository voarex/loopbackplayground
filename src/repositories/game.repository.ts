import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Game, GameRelations, Score} from '../models';
import {ScoreRepository} from './score.repository';

export class GameRepository extends DefaultCrudRepository<
  Game,
  typeof Game.prototype.id,
  GameRelations
> {

  public readonly scores: HasManyRepositoryFactory<Score, typeof Game.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('ScoreRepository') protected scoreRepositoryGetter: Getter<ScoreRepository>,
  ) {
    super(Game, dataSource);
    this.scores = this.createHasManyRepositoryFactoryFor('scores', scoreRepositoryGetter,);
    this.registerInclusionResolver('scores', this.scores.inclusionResolver);
  }
}
