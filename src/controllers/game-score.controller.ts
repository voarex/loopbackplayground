import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Game,
  Score,
} from '../models';
import {GameRepository} from '../repositories';

export class GameScoreController {
  constructor(
    @repository(GameRepository) protected gameRepository: GameRepository,
  ) { }

  @get('/games/{id}/scores', {
    responses: {
      '200': {
        description: 'Array of Game has many Score',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Score)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Score>,
  ): Promise<Score[]> {
    return this.gameRepository.scores(id).find(filter);
  }

  @post('/games/{id}/scores', {
    responses: {
      '200': {
        description: 'Game model instance',
        content: {'application/json': {schema: getModelSchemaRef(Score)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Game.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Score, {
            title: 'NewScoreInGame',
            exclude: ['id'],
            optional: ['gameId']
          }),
        },
      },
    }) score: Omit<Score, 'id'>,
  ): Promise<Score> {
    return this.gameRepository.scores(id).create(score);
  }

  @patch('/games/{id}/scores', {
    responses: {
      '200': {
        description: 'Game.Score PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Score, {partial: true}),
        },
      },
    })
    score: Partial<Score>,
    @param.query.object('where', getWhereSchemaFor(Score)) where?: Where<Score>,
  ): Promise<Count> {
    return this.gameRepository.scores(id).patch(score, where);
  }

  @del('/games/{id}/scores', {
    responses: {
      '200': {
        description: 'Game.Score DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Score)) where?: Where<Score>,
  ): Promise<Count> {
    return this.gameRepository.scores(id).delete(where);
  }
}
