import {Entity, model, property, hasMany} from '@loopback/repository';
import {Score} from './score.model';

@model()
export class Game extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  desc?: string;

  @hasMany(() => Score)
  scores: Score[];

  constructor(data?: Partial<Game>) {
    super(data);
  }
}

export interface GameRelations {
  // describe navigational properties here
}

export type GameWithRelations = Game & GameRelations;
