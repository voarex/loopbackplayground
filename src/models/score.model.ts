import {Entity, model, property} from '@loopback/repository';

@model()
export class Score extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'number',
    required: true,
  })
  points: number;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  ts?: Date;

  @property({
    type: 'number',
  })
  gameId?: number;

  constructor(data?: Partial<Score>) {
    super(data);
  }
}

export interface ScoreRelations {
  // describe navigational properties here
}

export type ScoreWithRelations = Score & ScoreRelations;
